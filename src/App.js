import { useState } from "react";
import { ToastContainer } from "react-toastify";
import Header from "./components/Header";
import Main from "./components/Main";
import LANGUAGES from "./constans/languages";
import THEMES from "./constans/themes";

const classes = { root: "flex flex-col h-screen min-h-full overflow-hidden" };

const getInitialTheme = () => {
  const theme = localStorage.getItem("theme");
  return theme || THEMES.VS;
};

function App() {
  const [theme, setTheme] = useState(getInitialTheme());
  const [language, setLanguage] = useState(LANGUAGES.YAML);

  const onThemeChange = (theme) => {
    setTheme(theme);
    localStorage.setItem("theme", theme);
  };

  return (
    <div className={classes.root}>
      <Header
        theme={theme}
        setTheme={onThemeChange}
        language={language}
        setLanguage={setLanguage}
      />
      <Main theme={theme} language={language} />
      <ToastContainer position="bottom-center" />
    </div>
  );
}

export default App;
