import axios from "axios";
const ROOT = `http://test.jsight.io:8080/`;

export const parse = (payload) =>
  axios
    .post(ROOT, payload)
    .then((response) => response.data)
    .catch((error) => {
      const message = error.response.data.Message;
      throw Error(message);
    });
