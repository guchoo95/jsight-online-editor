const THEMES = {
  VS: "vs",
  VS_DARK: "vs-dark",
  HC_BLACK: "hc-black",
  IPLASTIC: "iplastic",
  GITHUB: "github",
  SUNBURST: "sunburst",
  MONOKAI: "monokai",
  AMY: "amy",
};

export default THEMES;
