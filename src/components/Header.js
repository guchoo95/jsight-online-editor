import THEMES from "../constans/themes";
const THEMES_ARRAY = Object.values(THEMES);

const classes = {
  header: "p-3 flex justify-end text-white bg-purple-700",
  block: "flex text-sm",
  select: "bg-white text-black",
};

function ChooseTheme({ theme, setTheme }) {
  return (
    <select
      value={theme}
      onChange={(e) => setTheme(e.target.value)}
      className={classes.select}
    >
      {THEMES_ARRAY.map((option) => (
        <option children={option} value={option} key={option} />
      ))}
    </select>
  );
}

const Header = ({ theme, setTheme }) => {
  return (
    <header className={classes.header}>
      <div className={classes.block}>
        <label>Theme</label>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <ChooseTheme theme={theme} setTheme={setTheme} />
      </div>
    </header>
  );
};

export default Header;
