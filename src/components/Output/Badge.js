import classNames from "classnames";

const classes = {
  base: "w-auto inline-flex text-white font-semibold rounded-md",
  badge: "py-2 px-7",
  smallBadge: "text-xs px-3 py-1",
};

export const Badge = ({ children, className = "bg-green" }) => (
  <span
    className={classNames(classes.base, classes.badge, className)}
    children={children}
  />
);

export const SmallBadge = ({ children, className = "bg-green" }) => (
  <span
    className={classNames(classes.base, classes.smallBadge, className)}
    children={children}
  />
);
