import { Fragment } from "react";

const classes = { divider: "border-1 border-dark-gama" };

const Divider = () => (
  <Fragment>
    <div className="pt-3" />
    <div className={classes.divider} />
    <div className="pt-6" />
  </Fragment>
);

export default Divider;
