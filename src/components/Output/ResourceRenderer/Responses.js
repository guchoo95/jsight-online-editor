import { SmallBadge } from "../Badge";
import ResourceBody from "./ResponseBody";

function propertiesToJSON(properties) {
  let json = {};
  if (properties) {
    for (const [key, value] of Object.entries(properties)) {
      json[key] = value.scalarValue;
    }
  }
  return json;
}

const Responses = ({ data, tags }) => {
  const tagsString = tags?.length ? tags.join(",") : "";

  return (
    <div className="grid gap-y-6">
      {data?.length
        ? data.map((response, key) => {
            const badgeClassName =
              response?.code >= 500 ? "bg-red" : "bg-green";
            const body = response?.body.schema.content.properties;
            const bodyJSON = propertiesToJSON(body);

            return (
              <div key={key.toString()}>
                <div className="bg-dark-delta p-2 flex items-center">
                  <SmallBadge
                    children={response.code}
                    className={badgeClassName}
                  />
                  <div className="pl-7" />
                  <span children={tagsString} />
                </div>
                {Object.keys(bodyJSON).length ? (
                  <ResourceBody content={JSON.stringify(bodyJSON, null, 2)} />
                ) : null}
              </div>
            );
          })
        : null}
    </div>
  );
};

export default Responses;
