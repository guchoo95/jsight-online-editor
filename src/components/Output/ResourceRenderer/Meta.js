import { Badge } from "../Badge";

const classes = { root: "flex items-center" };

const Meta = ({ data }) => {
  const classNameByMethod = {
    GET: "bg-violet-500",
    DELETE: "bg-red",
    POST: "bg-green",
    PUT: "bg-blue-500",
    PATCH: "bg-pink-500",
  };
  const badgeClassName = classNameByMethod?.[data.httpMethod];

  return (
    <div className={classes.root}>
      <Badge children={data.httpMethod} className={badgeClassName} />
      <div className="pl-5" />
      <span children={data.path} />
    </div>
  );
};

export default Meta;
