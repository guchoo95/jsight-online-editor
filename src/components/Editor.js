import * as monaco from "monaco-editor";
import { useEffect, useRef } from "react";
// EDITOR  THEMES
import github from "../constans/editorThemes/github.json";
import iplastic from "../constans/editorThemes/iplastic.json";
import sunburst from "../constans/editorThemes/sunburst.json";
import monokai from "../constans/editorThemes/monokai.json";
import amy from "../constans/editorThemes/amy.json";

function defineThemes() {
  monaco.editor.defineTheme("iplastic", iplastic);
  monaco.editor.defineTheme("github", github);
  monaco.editor.defineTheme("sunburst", sunburst);
  monaco.editor.defineTheme("monokai", monokai);
  monaco.editor.defineTheme("amy", amy);
}

function initializeEditor(ref, options) {
  return monaco.editor.create(ref, options);
}

function updateEditorOptions(editor, options) {
  return editor.updateOptions(options);
}

function getEditorValue(editor) {
  return editor?.getValue();
}

function onContentChange({ event, editor, setContent }) {
  const content = getEditorValue(editor);
  setContent(content);
}

const Editor = ({ theme, language, content, setContent, setParsedContent }) => {
  const ref = useRef();
  const jsightEditor = useRef(null);

  useEffect(() => {
    defineThemes();
  }, []);

  useEffect(() => {
    const editor = initializeEditor(ref.current, {
      value: content,
      theme,
      language,
      fontSize: 20,
      codeLens: false,
      scrollbar: { useShadows: false, vertical: "hidden" },
      contextmenu: false,
      renderLineHighlight: "none",
      minimap: { enabled: false },
      overviewRulerLanes: 0,
      wordBasedSuggestions: false,
    });

    editor
      .getModel()
      .onDidChangeContent((event) =>
        onContentChange({ event, editor, setContent })
      );
    jsightEditor.current = editor;
  }, []);

  useEffect(() => {
    updateEditorOptions(jsightEditor.current, { theme });
  }, [theme]);

  return <div className="h-full w-6/12" ref={ref} />;
};

export default Editor;
