import { useEffect, useMemo, useState } from "react";
import { useDebounce } from "../hooks/useDebounce";
import { toast } from "react-toastify";
import { parse } from "../services/parse";
import Editor from "./Editor";
import Output from "./Output/index";

export async function parseContent({ content, setParsedContent }) {
  try {
    const parsedContent = await parse(content);
    setParsedContent(parsedContent);
    toast.dismiss();
  } catch (error) {
    const message = error.message;
    toast.dismiss();
    toast.warning(message, {
      toastId: 1,
      hideProgressBar: true,
      autoClose: false,
    });
  }
}

const initialContent = `GET /cats

200
  {
      "id": 1,
      "name": "Tom",
      "age": 3
  }

  500
  {
      "error": true,
      "message": "Something went wrong"
  }

  POST /cats

200
  {
      "name": "Tom",
      "age": 3
  }

DELETE /cats/{id}

200
  {
      "id": 1
  }

  500
  {
      "error": true,
      "message": "Failed to delete entity"
  }

PUT /cats/{id}

200
  {
      "id": 1,
      "name": "Tom"
  }`;

const Main = ({ theme, language }) => {
  const [content, setContent] = useState(initialContent);
  const [parsedContent, setParsedContent] = useState();
  const contentDebounce = useDebounce(content, 200);

  // LISTEN TO CONTENT CHANGES WITH DEBOUNCE ,THEN PARSE
  useEffect(() => {
    parseContent({ content, setParsedContent });
  }, [contentDebounce]);

  const EditorComponent = useMemo(
    () => (
      <Editor
        theme={theme}
        language={language}
        content={content}
        setContent={setContent}
        setParsedContent={setParsedContent}
      />
    ),
    [theme, language, content]
  );

  const OutputComponent = useMemo(
    () => <Output parsedContent={parsedContent} />,
    [parsedContent]
  );

  return (
    <main className="flex-1 flex overflow-hidden">
      {EditorComponent}
      {OutputComponent}
    </main>
  );
};

export default Main;
